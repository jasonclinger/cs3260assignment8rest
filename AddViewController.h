//
//  AddViewController.h
//  Assignment8REST
//
//  Created by Jason Clinger on 3/1/16.
//  Copyright © 2016 Jason Clinger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AddViewController : UIViewController{
    
    NSString* addname;
    NSString* addcolor;
    
}

@property (weak, nonatomic) IBOutlet UITextField *textFieldName;

@property (weak, nonatomic) IBOutlet UITextField *textFieldColor;

- (IBAction)saveBtnTouched:(id)sender;

@end
